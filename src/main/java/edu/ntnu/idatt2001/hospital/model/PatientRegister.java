package edu.ntnu.idatt2001.hospital.model;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a patient register, responsible for storing patients.
 */
public class PatientRegister {
    private List<Patient> patients;

    /**
     * Creates a PatientRegister with a empty arraylist for storing patients
     */
    public PatientRegister() {
        patients = new ArrayList<>();
    }

    /** Returns list of patients */
    public List<Patient> getPatients() {
        return patients;
    }

    /** Sets the list of patients */
    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    /**
     * Adds a patient to the patient register.
     *
     * @param patient Patient to be added
     */
    public void addPatient(Patient patient) {
        patients.add(patient);
    }

    /**
     * Removes a patient from the patient register.
     *
     * @param patient Patient to be removed
     */
    public void removePatient(Patient patient) {
        patients.remove(patient);
    }
}

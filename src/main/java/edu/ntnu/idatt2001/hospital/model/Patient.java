package edu.ntnu.idatt2001.hospital.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a patient with fields first name, last name,
 * social security number, diagnosis and name of the patients general practitioner.
 */
public class Patient {
    private StringProperty firstName;
    private StringProperty lastName;
    private StringProperty socialSecurityNumber;
    private StringProperty diagnosis;
    private StringProperty generalPractitioner;

    /**
     * Empty constructor
     */
    public Patient() {
        this.firstName = new SimpleStringProperty("");
        this.lastName = new SimpleStringProperty("");
        this.socialSecurityNumber = new SimpleStringProperty("");
        this.diagnosis = new SimpleStringProperty("");
        this.generalPractitioner = new SimpleStringProperty("");
    }
    /**
     * Creates a patient object with first name, last name and social security number.
     *
     * @param firstName The patients first name
     * @param lastName The patients last name
     * @param socialSecurityNumber The patients social security number
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.socialSecurityNumber = new SimpleStringProperty(socialSecurityNumber);
        this.diagnosis = new SimpleStringProperty("");
        this.generalPractitioner = new SimpleStringProperty("");
    }

    /**
     * Creates a patient object with first name, last name,social security number,
     * diagnosis and name of the patients general practitioner.
     *
     * @param firstName The patients first name
     * @param lastName The patients last name
     * @param socialSecurityNumber The patients social security number
     * @param diagnosis The diagnosis of the patient
     * @param generalPractitioner The general practitioner of the patient
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber, String diagnosis, String generalPractitioner) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.socialSecurityNumber = new SimpleStringProperty(socialSecurityNumber);
        this.diagnosis = new SimpleStringProperty(diagnosis);
        this.generalPractitioner = new SimpleStringProperty(generalPractitioner);
    }

    /** Returns the patients first name */
    public String getFirstName() {
        return firstName.get();
    }

    /** Sets the patients first name */
    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    /** Returns the patients last name */
    public String getLastName() {
        return lastName.get();
    }

    /** Sets the patients last name */
    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    /** Returns the patients social security number */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber.get();
    }

    /** Sets the patients social security number */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber.set(socialSecurityNumber);
    }

    /** Returns the patients diagnosis */
    public String getDiagnosis() {
        return diagnosis.get();
    }

    /** Sets the patients diagnosis */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis.set(diagnosis);
    }

    /** Returns the name of the patients general practitioner */
    public String getGeneralPractitioner() {
        return generalPractitioner.get();
    }

    /** Sets the name of the patients general practitioner */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner.set(generalPractitioner);
    }

    /** Returns StringProperty of the patients first name */
    public StringProperty firstNameProperty() {
        return firstName;
    }

    /** Returns StringProperty of the patients last name */
    public StringProperty lastNameProperty() {
        return lastName;
    }

    /** Returns StringProperty of the patients social security number */
    public StringProperty socialSecurityNumberProperty() {
        return socialSecurityNumber;
    }

    /** Returns StringProperty of the patients diagnosis */
    public StringProperty diagnosisProperty() {
        return diagnosis;
    }

    /** Returns StringProperty of the patients general practitioner */
    public StringProperty generalPractitionerProperty() {
        return generalPractitioner;
    }

    /** Returns a String representation of a Patient object of the form:
     *  Full name. (socialSecurityNumber)*/
    public String toString() {
        return firstName.get() + " " + lastName.get() + ". (" + socialSecurityNumber.get() + ")";
    }
}

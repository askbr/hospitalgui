package edu.ntnu.idatt2001.hospital.controller;

import edu.ntnu.idatt2001.hospital.model.Patient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import static edu.ntnu.idatt2001.hospital.utilities.StringUtilities.isStringNullOrEmpty;
import static edu.ntnu.idatt2001.hospital.utilities.StringUtilities.isStringOnlyDigits;

/**
 * This class is the controller of modify patient windows, including add and edit patient windows.
 */
public class ModifyPatientController{

    /**
     * Instead of having a mainController variable, methods in this class could
     * have been defined in MainController (via EventHandlers). This would decrease
     * coupling between the two classes, but decrease cohesion. I choose to do it this way
     * to keep high cohesion as i dont think low coupling is of highest priority between
     * the two controllers. To make it as safe as possible, mainControllers only have three
     * public methods made to interact with the controller.
     *
     * Other solutions may exist, but these are the ones i considered.
     */
    private MainController mainController; //Used to update TableView in main controller
    private Patient patient;

    @FXML private BorderPane rootPane;
    @FXML private TextField firstNameField;
    @FXML private TextField lastNameField;
    @FXML private TextField socialSecurityNumberField;

    /** Sets the patient to be modified and its fields into the UI */
    public void setPatient(Patient patient) {
        this.patient = patient;
        setPatientFields(patient);
    }

    /** Sets the main controller */
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    /**
     * Closes the modify patient window.
     * This method is called when a user clicks the "cancel" button.
     *
     * @param event ActionEvent sent when user clicks "Cancel"
     */
    @FXML
    private void cancel(ActionEvent event) {
        closeModifyPatientStage();
    }

    /**
     * Saves the modified patient in the applications patient register (stored in MainController).
     *
     * @param event ActionEvent sent when user clicks  "Save"
     */
    @FXML
    private void save(ActionEvent event) {
        boolean success = validateInput();

        if (success) { //Input is valid.
            if (patient == null) { //New patient
                patient = new Patient(firstNameField.getText(), lastNameField.getText(), socialSecurityNumberField.getText());

                mainController.registerNewPatient(patient);

            } else { //Patient edited
                patient.setFirstName(firstNameField.getText());
                patient.setLastName(lastNameField.getText());
                patient.setSocialSecurityNumber(socialSecurityNumberField.getText());

                mainController.registerPatientChanges(patient);

            }
            closeModifyPatientStage();
        }
    }

    /** Closes the stage of this controller. */
    private void closeModifyPatientStage() {
        Stage stage = (Stage) rootPane.getScene().getWindow();
        stage.close();
    }

    /**
     * Validates input in the textFields when modifying a patient.
     *
     * @return False if: no fields are empty or null,
     *         socialSecurityNumber does not have length 11 or consists any letters.
     *         Else, the method returns true.
     */
    private boolean validateInput() {
        String firstNameFieldInput = firstNameField.getText();
        String lastNameFieldInput = lastNameField.getText();
        String socialSecurityNumberFieldInput = socialSecurityNumberField.getText();

        if (isStringNullOrEmpty(firstNameFieldInput)) {
            displayInputError("First Name cannot be empty");
            return false;
        }

        if (isStringNullOrEmpty(lastNameFieldInput)) {
            displayInputError("Last Name cannot be empty");
            return false;
        }

        if (isStringNullOrEmpty(socialSecurityNumberFieldInput)) {
            displayInputError("Social Security Number is required");
            return false;
        }

        if (socialSecurityNumberFieldInput.length() != 11) { //Social security number should be 11 digits.
            displayInputError("Social Security Number should be 11 digits");
            return false;
        }

        if (!isStringOnlyDigits(socialSecurityNumberFieldInput)) {
            displayInputError("Social security Number should only contain digits");
            return false;
        }
        return true;
    }

    /**
     * Displays a input error dialog.
     *
     * @param error Error message of the specific input error made
     */
    private void displayInputError(String error) {
        Alert errorDialog = new Alert(Alert.AlertType.ERROR);
        errorDialog.setTitle("Input Error");
        errorDialog.setHeaderText(error);
        errorDialog.show();
    }

    /**
     * Sets the modifyPatient window text fields.
     *
     * @param patient Patient to be displayed in the modifyPatient window text fields
     */
    private void setPatientFields(Patient patient) {
        firstNameField.setText(patient.getFirstName());
        lastNameField.setText(patient.getLastName());
        socialSecurityNumberField.setText(patient.getSocialSecurityNumber());
    }

}
package edu.ntnu.idatt2001.hospital.controller;


import edu.ntnu.idatt2001.hospital.dao.CSVHandler;
import edu.ntnu.idatt2001.hospital.exception.CSVFormatException;
import edu.ntnu.idatt2001.hospital.model.Patient;
import edu.ntnu.idatt2001.hospital.model.PatientRegister;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This class is the controller of the main window.
 */
public class MainController implements Initializable {

    private final static String MODIFY_PATIENT_FXML_PATH =
            "src/main/resources/edu/ntnu/idatt2001/hospital/view/ModifyPatient.fxml";

    @FXML private TableView<Patient> patientTable;
    @FXML private TableColumn<Patient, String> firstNameColumn;
    @FXML private TableColumn<Patient, String> lastNameColumn;
    @FXML private TableColumn<Patient, String> socialSecurityNumberColumn;
    @FXML private Label statusLabel;

    private PatientRegister patientRegister;
    private ObservableList<Patient> patientObservableList;


    /**
     * Called to initialize a controller after its root element has been completely processed.
     *
     * @param url The location used to resolve relative paths for the root object,
     *           or null if the location is not known.
     *
     * @param resourceBundle The resources used to localize the root object,
     *                      or null if the root object was not localized.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Setting up columns of the tableview
        firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        socialSecurityNumberColumn.setCellValueFactory(cellData -> cellData.getValue().socialSecurityNumberProperty());

        //Setting up the patient register and observable list
        patientRegister = new PatientRegister();
        patientObservableList = FXCollections.observableArrayList(patientRegister.getPatients());
        patientTable.setItems(patientObservableList);

        updateStatus("Application running...");
    }

    /**
     * Sets up and displays a window for adding a patient to the patient register.
     * This method is called when a user clicks the option "Add Patient".
     *
     * @param event ActionEvent sent when user clicks option "Add Patient"
     */
    @FXML
    private void displayAddPatient(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(new File(MODIFY_PATIENT_FXML_PATH).toURI().toURL());
            Parent root = fxmlLoader.load();

            //Setting up view controller
            ModifyPatientController modifyPatientController = fxmlLoader.getController();
            modifyPatientController.setMainController(this);

            //Setting up stage
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Add Patient");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        } catch (IOException e) {
            updateStatus("IOException occurred when loading the add patient window.");
        }
    }

    /**
     * Sets up and displays a window for editing a patient of the patient register.
     * This method is called when a user clicks the option "Edit Selected Patient".
     *
     * @param event ActionEvent sent when user clicks option "Edit Selected Patient"
     */
    @FXML
    private void displayEditPatient(ActionEvent event) {
        Patient selectedPatient = patientTable.getSelectionModel().getSelectedItem();

        if (selectedPatient == null) { // selectedPatient is null when no patient is selected
            displayPatientNotSelected();

        } else {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(new File(MODIFY_PATIENT_FXML_PATH).toURI().toURL());
                Parent root = fxmlLoader.load();

                //Setting up view controller
                ModifyPatientController modifyPatientController = fxmlLoader.getController();
                modifyPatientController.setPatient(selectedPatient);
                modifyPatientController.setMainController(this);

                //Setting up stage
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.setTitle("Edit Patient");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();

            } catch(IOException e) {
                updateStatus("Error: Edit Patient window could not be loaded.");
            }
        }
    }

    /**
     * Sets up and displays a dialog for removing a selected patient from the patient register.
     * This method is called when a user clicks the option "Remove Selected Patient".
     *
     * @param event ActionEvent sent when user clicks option "Remove Selected Patient"
     */
    @FXML
    private void displayRemovePatient(ActionEvent event) {
        Patient selectedPatient = patientTable.getSelectionModel().getSelectedItem();

        if (selectedPatient == null) { // selectedPatient is null when no patient is selected
            displayPatientNotSelected();
        } else {
            Alert confirmationDialog = new Alert(Alert.AlertType.CONFIRMATION);
            confirmationDialog.setTitle("Confirm Action");
            confirmationDialog.setHeaderText("Delete confirmation");
            confirmationDialog.setContentText("Are you sure you want to delete this patient?");

            confirmationDialog.showAndWait().ifPresent(response -> {
                if (response == ButtonType.OK) {
                    patientRegister.removePatient(selectedPatient);
                    updateTableView();
                    updateStatus("Patient " + selectedPatient + " was successfully removed.");
                }
            });
        }
    }

    /**
     * Displays a window for exporting a CSV file to a desired location on the users computer.
     * This method is called when a user clicks the option "Export CSV".
     *
     * @param event ActionEvent sent when user clicks option "Export CSV"
     */
    @FXML
    private void displayExportCSV(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();

        //Adding extension filter restricting the user to only saving file as *.csv
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV file", "*.csv"));
        File file = fileChooser.showSaveDialog(new Stage());


        if (file != null) {
            CSVHandler.savePatientsToCSV(file, patientRegister.getPatients());
            updateStatus("Patient register was exported to CSV File: " + file.getName());
        }
    }

    /**
     * Displays a window for importing a CSV file from a desired location on the users computer.
     * This method is called when a user clicks the option "Import CSV".
     *
     * @param event ActionEvent sent when user clicks option "Import CSV"
     */
    @FXML
    private void displayImportCSV(ActionEvent event) {
        FileChooser fc = new FileChooser();

        //Adding extension filter restricting the user to only import file as *.csv
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files" , "*.csv"));
        File file = fc.showOpenDialog(new Stage());
        if (file != null) {
            try {
                patientRegister.setPatients(CSVHandler.getPatientsFromCSV(file));
                updateTableView();
                updateStatus("CSV File: " + file.getName() + " was loaded.");

            } catch (CSVFormatException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    /**
     * Displays a window with information about the Hospital GUI application.
     * This method is called when a user clicks the option "About".
     *
     * @param event ActionEvent sent when user clicks option "About"
     */
    @FXML
    private void displayAbout(ActionEvent event) {
        Alert informationDialog = new Alert(Alert.AlertType.INFORMATION);
        informationDialog.setTitle("About");
        informationDialog.setHeaderText("Patient Register Application");
        informationDialog.setContentText("Created by Ask Brandsnes Røsand.");
        informationDialog.show();
    }

    /**
     * Exits the main window of the GUI.
     * This method is called when a user clicks the option "Exit".
     *
     * @param event ActionEvent sent when user clicks option "Exit"
     */
    @FXML
    private void exitApplication(ActionEvent event) {
        Stage stage = (Stage) patientTable.getScene().getWindow();
        stage.close();
    }


    /**
     * Updates the tableview, syncing it with
     * the patients registered in field "patientRegister".
     */
    private void updateTableView() {
        patientObservableList.setAll(patientRegister.getPatients());
    }


    /**
     * Registers a new patient to the patient register (field patientRegister)
     *
     * @param patient Patient to be registered
     */
    public void registerNewPatient(Patient patient) {
        this.patientRegister.addPatient(patient);
        updateStatus("Patient " + patient + " was added to the patient register.");
        updateTableView();
    }

    /**
     * Calling this method informs the controller about a patient in the tableview has been changed.
     * This method should be called after editing one of the registered patients.
     *
     * @param patient Patient which was edited
     */
    public void registerPatientChanges(Patient patient) {
        updateStatus("Patient " + patient + " was edited.");
        updateTableView();
    }

    /**
     * Displays error dialog informing the user a patient is not selected.
     * This method is called when a user tries to edit/remove a patient without selecting one.
     */
    private void displayPatientNotSelected() {
        Alert errorDialog = new Alert(Alert.AlertType.ERROR);
        errorDialog.setTitle("No Patient selected");
        errorDialog.setHeaderText("You have to select a patient in order to edit or remove the patient.");
        errorDialog.show();
    }

    /**
     * Updates the status label in the application.
     *
     * @param status The new status to be displayed
     */
    private void updateStatus(String status) {
        statusLabel.setText(status);
    }
}

package edu.ntnu.idatt2001.hospital.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;

/**
 * JavaFX App
 */
public class App extends Application {

    //Constants
    private static final Logger LOGGER = Logger.getLogger(App.class.getName());
    private static final String MAIN_FXML_PATH = "src/main/resources/edu/ntnu/idatt2001/hospital/view/Main.fxml";
    private static final String MAIN_WINDOW_ICON_PATH = "src/main/resources/edu/ntnu/idatt2001/hospital/image/icon/Hospital.png";
    private static final String MAIN_WINDOW_TITLE = "Hospital GUI";
    private static final int DEFAULT_WIDTH = 900;
    private static final int DEFAULT_HEIGHT = 600;

    /**
     * Called when JavaFX applications is launched
     *
     * @param stage Primary Stage
     */
    @Override
    public void start(Stage stage) {
        try {
            //Loading FXML
            FXMLLoader fxmlLoader = new FXMLLoader(new File(MAIN_FXML_PATH).toURI().toURL());
            Parent root = fxmlLoader.load();

            //Setting up scene and stage
            Scene scene = new Scene(root, DEFAULT_WIDTH, DEFAULT_HEIGHT);
            stage.setScene(scene);
            stage.setTitle(MAIN_WINDOW_TITLE);
            setStageIcon(stage);

            stage.show();
            LOGGER.info("Main window successfully loaded.");
        } catch(IOException e) {
            LOGGER.severe("IOException occurred when loading the main window.");
        }
    }

    /** Sets the stage icon of the main window */
    private static void setStageIcon(Stage stage) {
        try {
            URL url = new File(MAIN_WINDOW_ICON_PATH).toURI().toURL();
            stage.getIcons().add(new Image(String.valueOf(url)));
            LOGGER.fine("Icon for main window successfully loaded.");
        } catch (IOException e) {
            LOGGER.warning("IOException occurred when loading the icon for the main window.");
        }
    }

    /** Main class */
    public static void main(String[] args) {
        launch();
    }

}
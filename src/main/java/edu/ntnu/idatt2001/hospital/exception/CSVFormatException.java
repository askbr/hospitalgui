package edu.ntnu.idatt2001.hospital.exception;

/**
 * The class CSVFormatException, subclass of Exception is thrown
 * to indicate that a CSV file os is not formatted correctly
 *
 * CSVFormatException is a checked exception.
 */
public class CSVFormatException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new CSVException with the specified detail message
     *
     * @param message The detail message
     */
    public CSVFormatException(String message) {
        super(message);
    }

}

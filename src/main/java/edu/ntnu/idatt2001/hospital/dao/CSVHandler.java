package edu.ntnu.idatt2001.hospital.dao;

import edu.ntnu.idatt2001.hospital.exception.CSVFormatException;
import edu.ntnu.idatt2001.hospital.model.Patient;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * This class handles Input and Output from CSV files.
 */
public class CSVHandler {
    private static final Logger LOGGER = Logger.getLogger(CSVHandler.class.getName());
    private static final String CSV_FIRST_LINE = "firstName;lastName;generalPractitioner;socialSecurityNumber";
    private static final String CSV_SEPARATOR = ";";

    /**
     * Empty Constructor
     */
    private CSVHandler() {

    }

    /**
     * Gets pateints from a CSV file.
     *
     * @param csv CSV File with patients to be imported
     * @return List of Patients retrieved from specific CSV file
     */
    public static List<Patient> getPatientsFromCSV(File csv) throws CSVFormatException {

        ArrayList<Patient> patients = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(csv))) {

            String line = bufferedReader.readLine(); // Initializing line variable and skipping the first line in csv

            if (line.equalsIgnoreCase(CSV_FIRST_LINE)) {
                while ((line = bufferedReader.readLine()) != null) {
                    String[] lineArray = line.split(CSV_SEPARATOR);

                    if (lineArray.length < 4) { //Indicating: there exists a cell in the CSV with no value.
                        throw new CSVFormatException(
                                "CSV file not correctly formatted. No column can have empty cells.");
                    }

                    Patient newPatient = new Patient();
                    newPatient.setFirstName(lineArray[0]);
                    newPatient.setLastName(lineArray[1]);
                    newPatient.setGeneralPractitioner(lineArray[2]);
                    newPatient.setSocialSecurityNumber(lineArray[3]);

                    patients.add(newPatient);
                }

            } else {
                throw new CSVFormatException("CSV file not correctly formatted. The first line of the csv file" +
                        " should be: \"firstName;lastName;generalPractitioner;socialSecurityNumber\"");
            }

        } catch(IOException e){
            LOGGER.warning("Error when loading Patients from CSV File.");
        }

        return patients;
    }

    /**
     * Saves current patientRegister of the Hospital GUI locally.
     *
     * @param csv CSV file where patients should be exported
     * @param patients List of patients which shall be exported
     */
    public static void savePatientsToCSV(File csv, List<Patient> patients) {
        try (FileWriter fileWriter = new FileWriter(csv);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            PrintWriter printWriter = new PrintWriter(bufferedWriter)) {

            printWriter.println(CSV_FIRST_LINE);

            for (Patient p : patients) {
                printWriter.println(p.getFirstName()
                        + CSV_SEPARATOR + p.getLastName()
                        + CSV_SEPARATOR + p.getGeneralPractitioner()
                        + CSV_SEPARATOR + p.getSocialSecurityNumber());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

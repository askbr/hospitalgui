package edu.ntnu.idatt2001.hospital.factory;

import edu.ntnu.idatt2001.hospital.model.Patient;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.io.File;
import java.net.URL;
import java.util.logging.Logger;

/**
 * This class is a factory class for the GUI components used in this application
 * inheriting from the abstract class javafx.scene.Node.
 */
public class NodeFactory {
    private static final Logger LOGGER = Logger.getLogger(NodeFactory.class.getName());

    /**
     * Creates nodes of specified type
     * @param type Type of node, currently supporting:
     *             TableView, MenuBar, ToolBar, VBox and BorderPane
     *
     * @return Node of specified type
     */
    public Node createNode(String type) {
        if (type.equalsIgnoreCase("tableview")) {
            return createTableView();
        } else if (type.equalsIgnoreCase("menubar")) {
            return createMenuBar();
        } else if (type.equalsIgnoreCase("toolbar")) {
            return createToolBar();
        } else if (type.equalsIgnoreCase("vbox")) {
            return new VBox();
        } else if (type.equalsIgnoreCase("borderpane")) {
            return new BorderPane();
        }

        return null;
    }

    /**
     * Creates a tableview for displaying patients
     * and their first name, last name and social security number.
     */
    private Node createTableView() {
        //Creating columns for the tableview
        TableColumn<Patient, String> firstNameColumn = new TableColumn<>("First Name");
        TableColumn<Patient, String> lastNameColumn = new TableColumn<>("Last Name");
        TableColumn<Patient, String> socialSecurityNumberColumn = new TableColumn<>("Social Security Number");

        //Setting up a tableView with the columns defined
        TableView<Patient> tableView = new TableView<>();
        tableView.getColumns().setAll(firstNameColumn, lastNameColumn, socialSecurityNumberColumn);
        return tableView;
    }

    /**
     * Creates a menubar which may be used for the hospitalGUI application.
     * The menubar consists of the three menus: File, Edit and Help.
     * Menu "File" consists of the menuItems: importCSV, exportCSV, exit
     * Menu "Edit" consists of the menuItems: addPatient, editSelectedPatient, removeSelectedPatient
     * Menu "Help" consists only of the menuItem about.
     *
     * @return MenuBar for the HospitalGUI application
     */
    private Node createMenuBar() {
        //Creating file menu for the menubar
        MenuItem importCSV = new MenuItem("Import from CSV...");
        MenuItem exportCSV = new MenuItem("Export to CSV");
        MenuItem exit = new MenuItem("Exit");

        Menu fileMenu = new Menu("File");
        fileMenu.getItems().addAll(importCSV, exportCSV, exit);

        //Creating edit menu for the menubar
        MenuItem addPatient = new MenuItem("Add new Patient...");
        MenuItem editSelectedPatient = new MenuItem("Edit selected Patient");
        MenuItem removeSelectedPatient = new MenuItem("Remove selected Patient");

        Menu editMenu = new Menu("Edit");
        editMenu.getItems().addAll(addPatient, editSelectedPatient, removeSelectedPatient);

        //Creating help menu for the menubar
        MenuItem about = new MenuItem("About");

        Menu helpMenu = new Menu("Help");
        helpMenu.getItems().addAll(about);

        return new MenuBar(fileMenu, editMenu, helpMenu);
    }

    /** Returns a toolbar for displaying options: "Add Patient", "Remove Patient", "Edit Patient". */
    private Node createToolBar() {
        final double ICON_WIDTH = 30;
        final double ICON_HEIGHT = 30;
        final String ICON_PACKAGE_PATH = "src/main/resources/edu/ntnu/idatt2001/hospital/image/icon";

        //Creating "Add Patient" button for the toolbar
        Button addPatientButton = new Button();
        try {
            URL urlAddPatient = new File(ICON_PACKAGE_PATH + "/Person_Add.png").toURI().toURL();
            ImageView addPersonIcon = new ImageView(String.valueOf(urlAddPatient));
            addPersonIcon.setFitHeight(ICON_HEIGHT);
            addPersonIcon.setFitWidth(ICON_WIDTH);
            addPatientButton.setGraphic(addPersonIcon);
        } catch (Exception e) {
            LOGGER.warning("Error occurred when creating the add Patient button for the toolbar.");
        }

        //Creating "Edit Selected Patient" button for the toolbar
        Button editPatientButton = new Button();
        try {
            URL urlEditPatient = new File(ICON_PACKAGE_PATH + "/Person_Manage.png").toURI().toURL();
            ImageView editPatientIcon = new ImageView(String.valueOf(urlEditPatient));
            editPatientIcon.setFitHeight(ICON_HEIGHT);
            editPatientIcon.setFitWidth(ICON_WIDTH);
            editPatientButton.setGraphic(editPatientIcon);

        } catch (Exception e) {
            LOGGER.warning("Error occurred when creating the edit Patient button for the toolbar.");
        }

        //Creating "Remove selected Patient" button for the toolbar
        Button removePatientButton = new Button();
        try {
            URL urlRemovePatient = new File(ICON_PACKAGE_PATH + "/Person_Remove.png").toURI().toURL();
            ImageView removePatientIcon = new ImageView(String.valueOf(urlRemovePatient));
            removePatientIcon.setFitHeight(ICON_HEIGHT);
            removePatientIcon.setFitWidth(ICON_WIDTH);
            removePatientButton.setGraphic(removePatientIcon);

        } catch (Exception e) {
            LOGGER.warning("Error occurred when creating the remove Patient button for the toolbar.");
        }

        return new ToolBar(addPatientButton, editPatientButton, removePatientButton);
    }


}

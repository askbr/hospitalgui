package edu.ntnu.idatt2001.hospital.utilities;

/**
 * This class contains utilities related to strings.
 */
public final class StringUtilities {

    private StringUtilities() {
    }

    /**
     * Checks if a String is either null or empty.
     *
     * @param string String to be checked
     * @return Returns true if String is either null or empty, otherwise false.
     */
    public static boolean isStringNullOrEmpty(String string) {
        return (string == null || string.isEmpty());
    }

    /**
     * Checks if a String contains only digits (0-9).
     *
     * @param string String to be checked
     * @return Returns true if String is only digits, otherwise false.
     */
    public static boolean isStringOnlyDigits(String string) {
        for (int i = 0; i<string.length(); i++) {
            char currentChar = string.charAt(i);
            if (currentChar > '9' || currentChar < '0') return false;
        }
        return true;
    }
}

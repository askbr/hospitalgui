module edu.ntnu.idatt2001.hospital {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;

    opens edu.ntnu.idatt2001.hospital.controller to javafx.fxml;
    exports edu.ntnu.idatt2001.hospital.controller;

    opens edu.ntnu.idatt2001.hospital.app to javafx.fxml;
    exports edu.ntnu.idatt2001.hospital.app;

    opens edu.ntnu.idatt2001.hospital.model to javafx.fxml;
    exports edu.ntnu.idatt2001.hospital.model;

    opens edu.ntnu.idatt2001.hospital.dao to javafx.fxml;
    exports edu.ntnu.idatt2001.hospital.dao;

    opens edu.ntnu.idatt2001.hospital.factory to javafx.fxml;
    exports edu.ntnu.idatt2001.hospital.factory;


}
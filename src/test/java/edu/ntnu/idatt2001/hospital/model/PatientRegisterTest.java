package edu.ntnu.idatt2001.hospital.model;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class is used for testing the PatientRegister class.
 */
@Nested
public class PatientRegisterTest {

    @Test
    @DisplayName("This test checks that the correct person is added to a PatientRegister " +
            "when using the method PatientRegister.addPatient() ")
    public void addPatientMethodDoesAddCorrectPatientToRegister() {
        Patient p1 = new Patient("Kjell", "Lauri", "123", "x", "Ove");
        PatientRegister register = new PatientRegister();
        register.addPatient(p1);

        assertEquals(register.getPatients().get(0), p1);
    }

    @Test
    @DisplayName("This test checks that the method PatientRegister.removePatient() " +
            "decreases the number of registered patients when an patient is removed.")
    public void removePatientMethodChangesNumberOfRegisteredPatients() {
        Patient p1 = new Patient("Siv", "Ola", "0910");
        Patient p2 = new Patient("Leo", "John", "92389");
        Patient p3 = new Patient("Samuel", "Larsen", "0320322");

        PatientRegister register = new PatientRegister();
        register.addPatient(p1);
        register.addPatient(p2);
        register.addPatient(p3);

        int patientsBeforePatientRemoved = register.getPatients().size();
        register.removePatient(p3); //Removes Patient p3.
        int patientsAfterPatientIsRemoved = register.getPatients().size();

        assertTrue(patientsBeforePatientRemoved > patientsAfterPatientIsRemoved);
    }

    @Test
    @DisplayName("This test checks that the method PatientRegister.removePatient() " +
            "removes the right patient from the register.")
    public void removeMethodDoesRemoveCorrectPatientFromRegister() {
        Patient p1 = new Patient("Even", "Odd", "1223");
        Patient p2 = new Patient("Liam", "Simen", "222");
        Patient p3 = new Patient("Fredrik", "Fredrikstad", "203982");
        PatientRegister register = new PatientRegister();

        register.addPatient(p1);
        register.addPatient(p2);
        register.addPatient(p3);

        register.removePatient(p3); //Removes Patient p3.

        List<Patient> patients = register.getPatients();
        for (Patient p : patients) { //Checks if Patient p3 still exists in department. Test fails if it does.
            assertNotEquals(p, p3);
        }
    }

    @Test
    @DisplayName("This test checks that the method PatientRegister.removePatient() " +
            "does not remove any patient which is not supposed to be removed.")
    public void removeMethodDoesntRemoveAnyOtherPatient() {
        Patient p1 = new Patient("Even", "Odd", "1223");
        Patient p2 = new Patient("Liam", "Simen", "222");
        Patient p3 = new Patient("Fredrik", "Fredrikstad", "203982");
        PatientRegister register = new PatientRegister();

        register.addPatient(p1);
        register.addPatient(p2);
        register.addPatient(p3);

        register.removePatient(p3); //Removes Patient p3.

        assertTrue(register.getPatients().contains(p1));
        assertTrue(register.getPatients().contains(p2));

    }
}

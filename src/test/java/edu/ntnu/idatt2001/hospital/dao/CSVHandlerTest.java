package edu.ntnu.idatt2001.hospital.dao;

import edu.ntnu.idatt2001.hospital.exception.CSVFormatException;
import edu.ntnu.idatt2001.hospital.model.Patient;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * This class is used for testing the CSVHandler class
 */
@Nested
public class CSVHandlerTest {

    @Test
    @DisplayName("Verifies that the first line of a CSV file is not included when" +
            " importing patients from a CSV file.")
    public void getPatientsFromCSVReturnsListWithoutFirstCSVLine() throws CSVFormatException {
        File csv = new File("src/test/resources/edu.ntnu.idatt2001.hospital.dao/Patients.csv");
        List<Patient> patients = CSVHandler.getPatientsFromCSV(csv);
        Patient firstPatient = patients.get(0);

        assertNotEquals("firstName", firstPatient.getFirstName());
        assertNotEquals("lastName", firstPatient.getLastName());
        assertNotEquals("generalPractitioner", firstPatient.getGeneralPractitioner());
        assertNotEquals("socialSecurityNumber", firstPatient.getSocialSecurityNumber());

    }

    @Test
    @DisplayName("Verifies that all Patients are imported from a CSV file.")
    public void getPatientsFromCSVImportsCorrectNumberOfPatients() throws CSVFormatException {
        File csv = new File("src/test/resources/edu.ntnu.idatt2001.hospital.dao/Patients.csv");
        List<Patient> patients = CSVHandler.getPatientsFromCSV(csv);

        assertEquals(104, patients.size());
    }

    @Test
    @DisplayName("Verifies that patient information from a CSV matches the patient objects " +
            "initialized from the CSV.")
    public void getPatientsFromCSVReturnsListWithCorrectPatientInformation() throws CSVFormatException{
        File csv = new File("src/test/resources/edu.ntnu.idatt2001.hospital.dao/Patients.csv");
        List<Patient> patients = CSVHandler.getPatientsFromCSV(csv);

        /*
        Randomly choosing five patients to check if they are correctly
        indexed and has the same fields as in the Patients.csv file.
         */

        //Patient 1 (List Index 0, CSV Row 2)
        Patient p1 = patients.get(0);
        assertEquals("Andrø", p1.getFirstName());
        assertEquals("Tanker", p1.getLastName());
        assertEquals("Maren G. Skake", p1.getGeneralPractitioner());
        assertEquals("29104300764", p1.getSocialSecurityNumber());


        //Patient 2 (List Index 27, CSV Row 29)
        Patient p2 = patients.get(27);
        assertEquals("Halvor Li", p2.getFirstName());
        assertEquals("Talt", p2.getLastName());
        assertEquals("Rally McBil", p2.getGeneralPractitioner());
        assertEquals("10084710356", p2.getSocialSecurityNumber());

        //Patient 3 (List Index 54, CSV Row 56)
        Patient p3 = patients.get(54);
        assertEquals("Kurt", p3.getFirstName());
        assertEquals("Isere", p3.getLastName());
        assertEquals("Edel Gran", p3.getGeneralPractitioner());
        assertEquals("20049920645", p3.getSocialSecurityNumber());

        //Patient 4 (List Index 89, CSV Row 91)
        Patient p4 = patients.get(89);
        assertEquals("Terry", p4.getFirstName());
        assertEquals("Bell", p4.getLastName());
        assertEquals("Matti Ompa", p4.getGeneralPractitioner());
        assertEquals("7088923736", p4.getSocialSecurityNumber());

        //Patient 5 (List Index 103, CSV Row 105)
        Patient p5 = patients.get(103);
        assertEquals("Unni", p5.getFirstName());
        assertEquals("Versell", p5.getLastName());
        assertEquals("Pøl Asset", p5.getGeneralPractitioner());
        assertEquals("2118126788", p5.getSocialSecurityNumber());
    }

    @Test
    @DisplayName("Verifies that when a wrongly formatted csv file is inputted to the application" +
            " an CSVFormatException is thrown.")
    public void wronglyFormattedCSVFileThrowsCSVFormatException() {
        File csv1 = new File("src/test/resources/edu.ntnu.idatt2001.hospital.dao/Patients2.csv");
        File csv2 = new File("src/test/resources/edu.ntnu.idatt2001.hospital.dao/Patients3.csv");

        assertThrows(CSVFormatException.class, () -> CSVHandler.getPatientsFromCSV(csv1));
        assertThrows(CSVFormatException.class, () -> CSVHandler.getPatientsFromCSV(csv2));
    }

}
